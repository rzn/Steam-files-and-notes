AppID: `238070`

You can use DOSBox to play this game on Linux.

A portable Linux build of DOSBox intended to be used for Shadow Warrior can be found at https://github.com/darealshinji/dosbox-trunk/releases
